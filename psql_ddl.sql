--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Ubuntu 11.5-0ubuntu0.19.04.1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-0ubuntu0.19.04.1)

-- Started on 2019-12-14 21:22:30 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE mydb;
--
-- TOC entry 2948 (class 1262 OID 16384)
-- Name: mydb; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE mydb WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE mydb OWNER TO postgres;

\connect mydb

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 16397)
-- Name: geo_location; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.geo_location (
    device_location_id integer NOT NULL,
    latitude text NOT NULL,
    longitude text NOT NULL,
    rua text NOT NULL,
    numero text NOT NULL,
    bairro text NOT NULL,
    cidade text NOT NULL,
    cep text NOT NULL,
    estado text NOT NULL,
    pais text NOT NULL,
    destination text
);


ALTER TABLE public.geo_location OWNER TO admin;

--
-- TOC entry 197 (class 1259 OID 16388)
-- Name: geo_points; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.geo_points (
    device_id integer NOT NULL,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    distance double precision NOT NULL,
    bearing double precision NOT NULL,
    created_on timestamp without time zone NOT NULL
);


ALTER TABLE public.geo_points OWNER TO admin;

--
-- TOC entry 196 (class 1259 OID 16386)
-- Name: geo_points_device_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.geo_points_device_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.geo_points_device_id_seq OWNER TO admin;

--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 196
-- Name: geo_points_device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.geo_points_device_id_seq OWNED BY public.geo_points.device_id;


--
-- TOC entry 2816 (class 2604 OID 16391)
-- Name: geo_points device_id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.geo_points ALTER COLUMN device_id SET DEFAULT nextval('public.geo_points_device_id_seq'::regclass);


--
-- TOC entry 2820 (class 2606 OID 16404)
-- Name: geo_location geo_location_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.geo_location
    ADD CONSTRAINT geo_location_pkey PRIMARY KEY (device_location_id);


--
-- TOC entry 2818 (class 2606 OID 16396)
-- Name: geo_points geo_points_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.geo_points
    ADD CONSTRAINT geo_points_pkey PRIMARY KEY (device_id);


--
-- TOC entry 2821 (class 2606 OID 16405)
-- Name: geo_location fk_geopoint_geolocation; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.geo_location
    ADD CONSTRAINT fk_geopoint_geolocation FOREIGN KEY (device_location_id) REFERENCES public.geo_points(device_id);


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 2948
-- Name: DATABASE mydb; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON DATABASE mydb TO admin;


-- Completed on 2019-12-14 21:22:30 -03

--
-- PostgreSQL database dump complete
--

