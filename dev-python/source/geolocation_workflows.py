import re
import pandas as pd
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
from datetime import datetime
from setup.connection import psqlengine
from utils.helpers import Helpers
from utils.query_sql import BuildSql


class Geolocation:

    def build_dataframe_datapoints(self, file):
        df = pd.DataFrame({'latitude': [], 'longitude': [], 'distance': [], 'bearing': [], 'created_on': []})
        non_decimal = re.compile(r'[^\d.]+')

        for i in range(len(file)):
            if file[i].lower().startswith('lat') and file[i + 1].lower().startswith('long') and file[ i + 2].lower().startswith('dist'):
                latitude = (file[i].split())[2]
                longitude = (file[i + 1].split())[2]
                distance = (file[i + 2].split())[1]
                bearing = non_decimal.sub('', (file[i + 2].split())[4])

                df = df.append({'latitude': latitude
                                   , 'longitude': longitude
                                   , 'distance': distance
                                   , 'bearing': bearing
                                   , 'created_on': datetime.now()}, ignore_index=True)
                i = i + 3

            elif file[i].lower().startswith('lat') and file[i + 1].lower().startswith('long') and (
            not file[i + 2].lower().startswith('dist')):
                latitude = (file[i].split())[2]
                longitude = (file[i + 1].split())[2]

                df = df.append({'latitude': latitude, 'longitude': longitude, 'distance': 0, 'bearing': 0,
                                'created_on': datetime.now()}, ignore_index=True)
                i = i + 2

            else:
                i = i + 1

        return df

    def insert_df_to_sql(self, table_name, df_file):
        df_file.to_sql(table_name, psqlengine(), if_exists='append', index=False)  #

    def build_dataframe_location(self):
        helper = Helpers();
        bsql = BuildSql();

        df = pd.DataFrame(
            {'device_location_id': [], 'latitude': [], 'longitude': [], 'rua': [], 'numero': [], 'bairro': [],
             'cidade': [], 'cep': [], 'estado': [], 'pais': [], 'destination': []})

        geolocator = Nominatim(user_agent="geo")
        rows = bsql.query_database(
            "SELECT device_id, latitude, longitude, distance, bearing FROM public.geo_points LIMIT 10");

        for row in rows:
            try:
                location = geolocator.reverse("{}, {}".format(row[1], row[2]), timeout=10, exactly_one=True)
                print(location)
                new_coordinates = helper.build_datapoint(row[1], row[2], row[3], row[4])
                destination = geolocator.reverse("{}, {}".format(new_coordinates[0], new_coordinates[1]), timeout=10
                                                 , exactly_one=True)

                df = df.append({'device_location_id': row[0]
                                   , 'latitude': location.latitude
                                   , 'longitude': location.longitude
                                   , 'rua': helper.get_component(location, 'road')
                                   , 'numero': helper.get_component(location, 'house_number')
                                   , 'bairro': helper.get_component(location, 'suburb')
                                   , 'cidade': helper.get_component(location, 'city')
                                   , 'cep': helper.get_component(location, 'postcode')
                                   , 'estado': helper.get_component(location, 'state')
                                   , 'pais': helper.get_component(location, 'country')
                                   , 'destination': destination.address}, ignore_index=True)

            except GeocoderTimedOut as e:
                print("Error: geocode failed on input %s with message %s" % (('LatLong:', row[1], row[2]), e.message))

        return df
