# -*- coding: utf-8 -*-

from utils.helpers import Helpers
from source.geolocation_workflows import Geolocation
import threading
from os import listdir
from os.path import isfile, join


def main(file_to_read):
    h = Helpers()
    geo_wf = Geolocation()

    data_points = h.read_txt_file(file_to_read);
    data_frame_dp = geo_wf.build_dataframe_datapoints(data_points);
    geo_wf.insert_df_to_sql('geo_points',data_frame_dp)
    data_frame_loc = geo_wf.build_dataframe_location()
    geo_wf.insert_df_to_sql('geo_location',data_frame_loc)


if __name__ == '__main__':
    data_points_files = [f for f in listdir('data') if isfile(join('data', f))]
    threads = len(data_points_files)

    jobs = []
    for i in range(0, threads):
        file = 'data/' + data_points_files[i]
        thread = threading.Thread(target=main(file))
        jobs.append(thread)

    for j in jobs:
        j.start()

    for j in jobs:
        j.join()
