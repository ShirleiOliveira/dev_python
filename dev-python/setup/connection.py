# -*- coding: utf-8 -*-

from configparser import ConfigParser
from sqlalchemy import create_engine
import json


def configpsql(filename='setup/database.ini', section='postgresql'):
    parser = ConfigParser()
    parser.read(filename)

    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


def psqlengine():
    with open('setup/database.json') as f:
        db = json.load(f)

    engine = create_engine(
        str(db['section']) + '://' + str(db['user']) + ':' + str(db['password']) + '@' + str(db['host']) + '/' + str(
            db['database']))
    return engine