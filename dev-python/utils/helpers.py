# -*- coding: utf-8 -*-
import math


class Helpers:

    def read_txt_file(self, file_name):
        rows = []
        with open(file_name, "r") as file:
            for line in file:
                rows.append(line)

        return rows

    def get_component(self, location, component_type):
        val = 'NA'
        for component in location.raw['address']:
            if component == component_type:
                val = location.raw['address'][component_type]
                break;

        return val

    def build_datapoint(self, lat, long, dist, bearing):
        R = 6378.1  # Radius of the Earth
        brng = bearing * (math.pi / 180)  # Bearing in degrees converted to radians.
        d = dist  # Distance in km

        lat1 = math.radians(lat)  # Current lat point converted to radians
        lon1 = math.radians(long)  # Current long point converted to radians

        lat2 = math.asin(math.sin(lat1) * math.cos(d / R) +
                         math.cos(lat1) * math.sin(d / R) * math.cos(brng))

        lon2 = lon1 + math.atan2(math.sin(brng) * math.sin(d / R) * math.cos(lat1),
                                 math.cos(d / R) - math.sin(lat1) * math.sin(lat2))

        lat2 = math.degrees(lat2)
        lon2 = math.degrees(lon2)
        return lat2, lon2
