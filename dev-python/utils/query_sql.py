# -*- coding: utf-8 -*-
import psycopg2
from setup.connection import configpsql


class BuildSql:

    def query_database(self, sql=""):
        rows = None
        try:
            params = configpsql()
            conn = psycopg2.connect(**params)
            cur = conn.cursor()
            cur.execute(sql)
            rows = cur.fetchall()

            return rows
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                cur.close()
                conn.close()
