# Desafio de Programação

Trata-se de um teste para avaliação de competências técnicas necessárias para o desenvolvimento de rotinas ETL utilizando Python como base.

## Sobre ETL

A sigla ETL vem do inglês Extract-Transform-Load e visa trabalhar com toda a parte de extração de dados de fontes externas, transformação para atender às necessidades de negócios e carga dos dados dentro do Data Warehouse.

Como podemos perceber, esse processo possui três etapas. A primeira é a extração (extract), a segunda a transformação (transform) e por fim, a carga (load). Cada uma delas possui grande importância para o sucesso da transição dos dados dos sistemas de origem para o DW.

> **Extração**: É a coleta de dados dos sistemas de origem (também chamados Data Sources ou sistemas operacionais), extraindo-os e transferindo-os para o ambiente de DW, onde o sistema de ETL pode operar independente dos sistemas operacionais.  
>  
> **Limpeza, Ajustes e Consolidação (ou também chamada transformação)**: É nesta etapa que realizamos os devidos ajustes, podendo assim melhorar a qualidade dos dados e consolidar dados de duas ou mais fontes. O estágio de transformação aplica um série de regras ou funções aos dados extraídos para ajustar os dados a serem carregados. Algumas fontes de dados necessitarão de muito pouca manipulação de dados. Em outros casos, podem ser necessários trabalhar algumas transformações, como por exemplo, Junção de dados provenientes de diversas fontes, seleção de apenas determinadas colunas e Tradução de valores codificados (se o sistema de origem armazena 1 para sexo masculino e 2 para feminino, mas o data warehouse armazena M para masculino e F para feminino, por exemplo).  
>   
> **Entrega ou Carga dos dados**: Consiste em fisicamente estruturar e carregar os dados para dentro da camada de apresentação seguindo o modelo dimensional. Dependendo das necessidades da organização, este processo varia amplamente. Alguns data warehouses podem substituir as informações existentes semanalmente, com dados cumulativos e atualizados, ao passo que outro DW (ou até mesmo outras partes do mesmo DW) podem adicionar dados a cada hora. A latência e o alcance de reposição ou acréscimo constituem opções de projeto estratégicas que dependem do tempo disponível e das necessidades de negócios.  
> 
> [Diagrama ETL](https://vivianeribeiro1.files.wordpress.com/2011/06/062811_1937_oqueetl1.jpg?w=469&h=321)

## O Problema Proposto

Possuímos arquivos de texto contendo uma lista de coordenadas geográficas obtidas a partir do GPS de dispositivos móveis. Precisamos que esses arquivos sejam processados, enriquecidos com informações disponíveis na internet e carregados em um RDBMS.

O objetivo do enriquecimento é descobrir os endereços correspondentes às coordenadas. Para essa finalidade, deve ser utilizada alguma API aberta como por exemplo o Google Maps.

Os dados enriquecidos são utilizados em nosso produto exponencial e de alta demanda, e pode ser necessário processar milhões de novas coordenadas por dia.

## Requisitos

1. A solução deverá ser implementada em Python 3 (de prefência a versão 3.6);
2. A solução como um todo deverá poder ser executada em ambiente Linux;
3. Todas as bibliotecas, produtos ou serviços utilizados deverão ser Open Source;
4. As configurações de conexão com banco de dados e APIs deverão ser parametrizadas;
5. Um script DDL deverá ser fornecido para a construção das estruturas do banco de dados;

## Insumos

Os dados contendo as coordenadas que serão necessárias para o desenvolvimento da solução estão disponíveis através do link [data_points](https://s3.amazonaws.com/dev.etl.python/datasets/data_points.tar.gz).

Os arquivos são compostos por uma série de coordenadas geográficas contendo:

- Latitude
- Longitude
- Distância

Em todas as linhas, os três atributos são apresentados em dois formatos:

- Em graus, minutos e segundos
- Em número decimal

Exemplo:
  
```text
Latitude: 30°02′59″S   -30.04982864  
Longitude: 51°12′05″W   -51.20150245  
Distance: 2.2959 km  Bearing: 137.352°  
```

## Resultados

Você é livre para criar (modelar) a estrutura no RDBMS para receber os dados enriquecidos, desde que o resultado final da visualização dos dados seja expresso neste formato:

| latitude  | 	longitude | 	rua 	| numero 	| bairro | 	cidade | 	cep 	| estado  | 	pais |
|-----------|-------------|-------------|-----------|--------|---------|------------|---------|----------|
| -30.027350  |	-51.182731 |	Rua Furriel Luíz Antônio de Vargas |	250 	| Bela Vista  |	Porto Alegre 	| 90470-130 |	RS |	Brasil |

## Avaliação

Os seguintes quesitos serão avaliados:

1. Estratégias de implementação e performance para solução do problema;
2. Padrões de Design e Padrões de Arquitetura;
3. Clareza, limpeza e organização do código;
4. Paradigma de programação (OOP);
5. Eficiência da implementação;
6. Testes e validações;
7. Documentação.

## Bônus

Além do enriquecimento proposto, que análises poderiam ser feitas utilizando essa massa de dados? Que métricas poderiam ser obtidas?